package com.wts.ecommerce.services;

import com.wts.ecommerce.api.v1.model.ProductDTO;
import com.wts.ecommerce.models.Product;

import java.util.List;

public interface ProductService {

    List<ProductDTO> findall();
    ProductDTO updateProduct(Long id, ProductDTO product);
    ProductDTO createProduct(ProductDTO product);
    ProductDTO findProductById(Long id);
    void deleteProductById(Long id);
    String getUrl(Long id);
}
