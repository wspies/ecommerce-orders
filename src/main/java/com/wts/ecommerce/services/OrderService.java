package com.wts.ecommerce.services;

import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.models.Order;

import java.util.Date;
import java.util.List;

public interface OrderService {

    List<OrderDTO> findBetweenDates(Date startDate, Date endDate);
    List<OrderDTO> findAll();
    OrderDTO findOrderById(Long id);
    OrderDTO createOrder(OrderDTO orderDTO);
    OrderDTO reCalculateOrderTotal(Long id);


}
