package com.wts.ecommerce.services.jpa;

import com.wts.ecommerce.api.v1.mapper.OrderMapper;
import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.api.v1.model.OrderitemDTO;
import com.wts.ecommerce.api.v1.model.ProductDTO;
import com.wts.ecommerce.controllers.v1.OrderController;
import com.wts.ecommerce.controllers.v1.ProductController;
import com.wts.ecommerce.models.Order;
import com.wts.ecommerce.models.Product;
import com.wts.ecommerce.repositories.OrderRepository;
import com.wts.ecommerce.services.OrderService;
import com.wts.ecommerce.services.ProductService;
import com.wts.ecommerce.services.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceJpa implements OrderService {


    private final OrderRepository orderRepository;
    private final ProductService productService;
    private final OrderMapper orderMapper;

    public OrderServiceJpa(OrderRepository orderRepository, OrderMapper orderMapper,ProductService productService) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.productService = productService;
    }

    @Override
    public List<OrderDTO> findBetweenDates(Date startDate, Date endDate) {

        return orderRepository.findByDateBetween(startDate,endDate)
                .stream()
                .map(order -> {
                    OrderDTO orderDTO = orderMapper.orderToOrderDto(order);
                    orderDTO.setOrderUrl(getUrl(orderDTO.getId()));
                    return orderDTO;
                })
                .map(orderDTO -> {
                    List<OrderitemDTO> orderitemDTO = orderDTO.getOrderItemDTO();
                    orderitemDTO.forEach(orderitemDTO1 -> {
                        orderitemDTO1.setProductUrl(productService.getUrl(orderitemDTO1.getProductId()));
                    });
                    return orderDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<OrderDTO> findAll() {
        return orderRepository.findAll()
                .stream()
                .map(order -> {
                    OrderDTO orderDTO = orderMapper.orderToOrderDto(order);
                    orderDTO.setOrderUrl(getUrl(orderDTO.getId()));
                    return orderDTO;
                })
                .map(orderDTO -> {
                    List<OrderitemDTO> orderitemDTO = orderDTO.getOrderItemDTO();
                    orderitemDTO.forEach(orderitemDTO1 -> {
                        orderitemDTO1.setProductUrl(productService.getUrl(orderitemDTO1.getProductId()));
                    });
                    return orderDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public OrderDTO findOrderById(Long id) {

        return orderRepository.findById(id)
                .map(orderMapper::orderToOrderDto)
                .map(orderDTO -> {
                    orderDTO.setOrderUrl(getUrl(orderDTO.getId()));
                    return orderDTO;
                })
                .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public OrderDTO createOrder(OrderDTO orderDTO) {

        /*
        * Process the items in the order
        * 1. get product info based on the product id
        * 2. store name,code, price in orderitemDTO
        * 3. save the order/orderitems
        * */

        List<OrderitemDTO> processItems = processOrderItems(orderDTO.getOrderItemDTO());

        Double orderTotal = calculateOrderTotalByProduct(processItems);
        orderDTO.setTotal(orderTotal);

        OrderDTO orderDtoReturn = saveOrder(orderDTO);
        orderDtoReturn.setOrderUrl(getUrl(orderDtoReturn.getId()));

        return orderDtoReturn;
    }

    @Override
    public OrderDTO reCalculateOrderTotal(Long id) {

        OrderDTO orderDTO = findOrderById(id);
        orderDTO.setTotal(calculateOrderTotalByOrderItem(orderDTO.getOrderItemDTO()));

        OrderDTO orderDtoReturn = saveOrder(orderDTO);
        orderDtoReturn.setOrderUrl(getUrl(orderDtoReturn.getId()));

        return orderDtoReturn;
    }

    private OrderDTO saveOrder(OrderDTO orderDTO) throws ResourceNotFoundException{

        Order order = orderMapper.orderDtoToOrder(orderDTO);
        Order savedOrder = orderRepository.save(order);

        return orderMapper.orderToOrderDto(savedOrder);
    }

    private List<OrderitemDTO> processOrderItems(List<OrderitemDTO> orderitemDTOList){

        orderitemDTOList.forEach(k-> {
               ProductDTO p = productService.findProductById(k.getProductId());
               k.setName(p.getName());
               k.setPrice(p.getPrice());
               k.setCode(p.getCode());
        });

        return orderitemDTOList;
    }

    /* Calculate the order total based on the product price and quantity, used when creating order */
    private double calculateOrderTotalByProduct(List<OrderitemDTO> orderitemDTOList){

        Double total = 0d;

        for (OrderitemDTO k : orderitemDTOList) {
            ProductDTO p = productService.findProductById(k.getProductId());
            total += p.getPrice() * k.getQuantity();
        }
        return total;
    }

    /* Calculate the order total based on the orderitem price and quantity*/
    private double calculateOrderTotalByOrderItem(List<OrderitemDTO> orderitemDTOList){

        Double total = 0d;

        for (OrderitemDTO k : orderitemDTOList) {
            total += k.getPrice() * k.getQuantity();
        }
        return total;
    }

    private String getUrl(Long id) {
        return OrderController.BASE_URL + "/" + id;
    }
}
