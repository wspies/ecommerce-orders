package com.wts.ecommerce.services.jpa;

import com.wts.ecommerce.api.v1.mapper.ProductMapper;
import com.wts.ecommerce.api.v1.model.ProductDTO;
import com.wts.ecommerce.controllers.v1.ProductController;
import com.wts.ecommerce.models.Product;
import com.wts.ecommerce.repositories.ProductRepository;
import com.wts.ecommerce.services.ProductService;
import com.wts.ecommerce.services.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceJpa implements ProductService {

    ProductRepository productRepository;
    ProductMapper productMapper;

    public ProductServiceJpa(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public List<ProductDTO> findall() {

        return productRepository.findAll()
                .stream()
                .map(product -> {
                    ProductDTO productDTO = productMapper.productToProductDto(product);
                    productDTO.setProductUrl(getUrl(productDTO.getId()));
                    return productDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public ProductDTO findProductById(Long id) {

        return productRepository.findById(id)
                .map(product -> {
                    ProductDTO productDTO = productMapper.productToProductDto(product);
                    productDTO.setProductUrl(getUrl(productDTO.getId()));
                    return productDTO;
                })
                .orElseThrow(ResourceNotFoundException::new);

    }

    @Override
    public ProductDTO createProduct(ProductDTO productDTO) {

        Product product = productMapper.productDtoToProduct(productDTO);
        Product savedProduct = productRepository.save(product);

        ProductDTO productDTOreturn = productMapper.productToProductDto(savedProduct);
        productDTOreturn.setProductUrl(getUrl(productDTOreturn.getId()));

        return productDTOreturn;
    }

    @Override
    public ProductDTO updateProduct(Long id, ProductDTO productDTO) {

        Product product = productMapper.productDtoToProduct(productDTO);
        product.setId(id);

        Product savedProduct = productRepository.save(product);

        ProductDTO productDTOreturn = productMapper.productToProductDto(savedProduct);
        productDTOreturn.setProductUrl(getUrl(productDTOreturn.getId()));

        return productDTOreturn;
    }

    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public String getUrl(Long id) {
        return ProductController.BASE_URL + "/" + id;
    }
}
