package com.wts.ecommerce.controllers.v1;

import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.api.v1.model.ProductDTO;
import com.wts.ecommerce.api.v1.model.ProductListDTO;
import com.wts.ecommerce.services.OrderService;
import com.wts.ecommerce.services.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(ProductController.BASE_URL)
public class ProductController {

    public static final String BASE_URL = "/api/v1/products";

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @ApiOperation(value = "Get all product")
    @GetMapping
    public ResponseEntity<ProductListDTO> getProducts(){

        return new ResponseEntity<ProductListDTO>(
                new ProductListDTO(productService.findall()),HttpStatus.OK);
    }

    @ApiOperation(value = "Get a product")
    @GetMapping({"/{id}"})
    public ResponseEntity<ProductDTO> getProductById(@PathVariable Long id){
        return new ResponseEntity<ProductDTO>(productService.findProductById(id),HttpStatus.OK);
    }

    @ApiOperation(value = "Create new product")
    @PostMapping
    public ResponseEntity<ProductDTO> createProduct(@RequestBody ProductDTO productDTO){
        return new ResponseEntity<ProductDTO>(productService.createProduct(productDTO),HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update a product")
    @PutMapping({"/{id}"})
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable Long id, @RequestBody ProductDTO productDTO){
        return new ResponseEntity<ProductDTO>(productService.updateProduct(id, productDTO),HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a product")
    @DeleteMapping({"/{id}"})
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id){
        productService.deleteProductById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
