package com.wts.ecommerce.controllers.v1;

import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.api.v1.model.OrderListDTO;
import com.wts.ecommerce.models.Order;
import com.wts.ecommerce.services.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping(OrderController.BASE_URL)
public class OrderController {

    public static final String BASE_URL = "/api/v1/orders";

    private final OrderService orderService;

    public OrderController(OrderService orderService) {

        this.orderService = orderService;
    }

    @ApiOperation(value = "Get all orders",
            notes ="Searching order between dates: use date format yyyy-MM-dd (e.g. 2018-01-31)")
    @GetMapping
    public ResponseEntity<OrderListDTO> getOrders(
            @RequestParam(name = "startdate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<Date> startDate,
            @RequestParam(name = "enddate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<Date> endDate
            ) throws ParseException
    {

        OrderListDTO orderListDTO;

        if (startDate.isPresent() && endDate.isPresent()){
            orderListDTO = new OrderListDTO(orderService.findBetweenDates(startDate.get(), endDate.get()));
        }
        else{
            orderListDTO = new OrderListDTO(orderService.findAll());
        }

        return new ResponseEntity<OrderListDTO>(orderListDTO,HttpStatus.OK);

    }

    @ApiOperation(value = "Get an order by ID")
    @GetMapping({"/{id}"})
    public ResponseEntity<OrderDTO> getOrderById(@PathVariable Long id){
        return new ResponseEntity<OrderDTO>(orderService.findOrderById(id),HttpStatus.OK);
    }

    @ApiOperation(value = "Create new order",
            notes =  "email field is mandatory. Order items only need a product id and a quantity")
    @PostMapping
    public ResponseEntity<OrderDTO> createOrder(@RequestBody OrderDTO orderDTO){
        return new ResponseEntity<OrderDTO>(orderService.createOrder(orderDTO),HttpStatus.CREATED);
    }

    @ApiOperation(value = "Recalculate order total based on the order items based on the order items")
    @PutMapping({"/{id}/calculate"})
    public ResponseEntity<OrderDTO> calculateOrderTotal(@PathVariable Long id){
        return new ResponseEntity<OrderDTO>(orderService.reCalculateOrderTotal(id),HttpStatus.OK);
    }

}
