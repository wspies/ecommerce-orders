package com.wts.ecommerce.api.v1.mapper;

import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.api.v1.model.ProductDTO;
import com.wts.ecommerce.models.Order;
import com.wts.ecommerce.models.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductDTO productToProductDto(Product product);
    Product productDtoToProduct(ProductDTO product);


}
