package com.wts.ecommerce.api.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wts.ecommerce.models.Orderitem;
import lombok.Data;

import java.util.Date;
import java.util.List;


@Data
public class OrderitemDTO {

    private Long id;

    private Long productId;

    private String code;

    private String name;

    private Integer quantity;

    private Double price;

    @JsonProperty("product_url")
    private String productUrl;


}
