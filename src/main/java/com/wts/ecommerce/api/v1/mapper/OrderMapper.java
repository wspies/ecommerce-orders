package com.wts.ecommerce.api.v1.mapper;

import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.models.Order;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    @Mappings({
        @Mapping(source="orderItemList", target="orderItemDTO")
    })
    OrderDTO orderToOrderDto(Order order);

    @Mappings({
            @Mapping(source="orderItemDTO", target="orderItemList"),
    })

    Order orderDtoToOrder(OrderDTO orderDTO);



}
