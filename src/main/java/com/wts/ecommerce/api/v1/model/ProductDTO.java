package com.wts.ecommerce.api.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ProductDTO {

    private Long id;

    private String code;

    private String name;

    private double price;

    @JsonProperty("product_url")
    private String productUrl;

}
