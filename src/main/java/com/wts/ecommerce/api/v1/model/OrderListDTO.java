package com.wts.ecommerce.api.v1.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Data
@AllArgsConstructor
public class OrderListDTO {
    private List<OrderDTO> orders;
}
