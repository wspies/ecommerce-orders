package com.wts.ecommerce.api.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Data
public class OrderDTO {

    private Long id;

    private Date date;

    private Double total;

    private String email;

    private List<OrderitemDTO> orderItemDTO;

    @JsonProperty("order_url")
    private String orderUrl;

    public List<OrderitemDTO> getOrderItemDTO() {

        if(orderItemDTO == null){
            return new LinkedList<OrderitemDTO>();
        }
        else{
            return orderItemDTO;
        }

    }
}
