package com.wts.ecommerce.api.v1.mapper;

import com.wts.ecommerce.api.v1.model.OrderitemDTO;
import com.wts.ecommerce.models.Orderitem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderitemMapper {

    OrderitemMapper INSTANCE = Mappers.getMapper(OrderitemMapper.class);

    OrderitemDTO orderItemToOrderItemDto(Orderitem orderitem);

}
