package com.wts.ecommerce.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Setter
@Getter
@Entity
@Table(name = "[order]")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double total;

    @CreationTimestamp
    private Date date;

    @NotNull(message = "Missing email")
    private String email;

    @OneToMany(cascade = CascadeType.ALL)
    private List <Orderitem> orderItemList;

}
