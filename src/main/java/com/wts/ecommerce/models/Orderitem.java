package com.wts.ecommerce.models;

import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name="orderitem")
public class Orderitem  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @NotNull(message = "Missing product")
    @Column(name="product_id")
    private Long productId;

    @Column(name="code")
    private String code;

    @Column(name="name")
    private String name;

    @NotNull(message = "No quantity specified")
    @Column(name="quantity")
    private Integer quantity;

    @Column(name="price")
    private Double price;

}
