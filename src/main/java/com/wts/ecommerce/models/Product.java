package com.wts.ecommerce.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
    @Setter
    @Entity
    @Table(name="product")
    public class Product {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="id")
        private Long id;

        @Column(name="code")
        private String code;

        @Column(name="name")
        private String name;

        @NotNull(message = "Missing price")
        @Column(name="price")
        private Double price;

}
