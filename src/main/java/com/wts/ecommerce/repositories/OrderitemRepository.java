package com.wts.ecommerce.repositories;

import com.wts.ecommerce.models.Orderitem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderitemRepository extends JpaRepository<Orderitem, Long> {
}
