package com.wts.ecommerce.repositories;

import com.wts.ecommerce.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository <Order,Long> {
    List<Order> findByDateBetween(Date startDate, Date endDate);
}
