insert into "order" ( date ,email ,total ) values ( '2018-01-03', 'test1@test.com',125.00) ;
insert into "order" ( date ,email ,total ) values ( '2018-01-015', 'test2@test.com',35.99) ;
insert into "order" ( date ,email ,total ) values ( '2018-02-04', 'test3@test.com',128.23) ;
insert into "order" ( date ,email ,total ) values ( '2018-03-03', 'test4@test.com',26.59) ;

insert into product (code, name, price) values  ('A123','Lorem ipsum dolor',12.99);
insert into product (code, name, price) values  ('A124','Amet consectetuer ',5.99);
insert into product (code, name, price) values  ('A125','Odio lacus lorem',7.39);
insert into product (code, name, price) values  ('A126','Feugiat adipiscing',25.79);
insert into product (code, name, price) values  ('A127','Tristique orci inceptos',98.12);

insert into orderitem(code, name, price, product_id, quantity) values  ('A123','Lorem ipsum dolor',12.99,1,3);
insert into orderitem(code, name, price, product_id, quantity) values  ('A124','Amet consectetuer',5.99,2,2);
insert into orderitem(code, name, price, product_id, quantity) values  ('A123','Odio lacus lorem',7.39,3,1);
insert into orderitem(code, name, price, product_id, quantity) values  ('A123','Feugiat adipiscing',25.79,4,5);

INSERT INTO "order_order_item_list" ("order_id",order_item_list_id) VALUES (1,1);
INSERT INTO "order_order_item_list" ("order_id",order_item_list_id) VALUES (1,2);
INSERT INTO "order_order_item_list" ("order_id",order_item_list_id) VALUES (2,3);
INSERT INTO "order_order_item_list" ("order_id",order_item_list_id) VALUES (2,4);