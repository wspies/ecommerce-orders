package com.wts.ecommerce.services.jpa;

import com.wts.ecommerce.api.v1.mapper.OrderMapper;
import com.wts.ecommerce.api.v1.mapper.ProductMapper;
import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.models.Order;
import com.wts.ecommerce.models.Orderitem;
import com.wts.ecommerce.models.Product;
import com.wts.ecommerce.repositories.OrderRepository;
import com.wts.ecommerce.repositories.ProductRepository;
import com.wts.ecommerce.services.OrderService;
import com.wts.ecommerce.services.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OrderServiceJpaTest {

    OrderService orderService;

    @Mock
    OrderRepository orderRepository;

    @Mock
    ProductService productService;

    OrderMapper orderMapper = OrderMapper.INSTANCE;
    ProductMapper productMapper = ProductMapper.INSTANCE;

    Order order1 = new Order();
    Order order2 = new Order();
    List<Order> orderList = new LinkedList<>();

    Calendar calendar = Calendar.getInstance();

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);
        orderService = new OrderServiceJpa(orderRepository,orderMapper,productService);

        calendar.set(2018,01,01,13,00);

        order1.setId(1l);
        order1.setTotal(5.25);
        order1.setDate(calendar.getTime());

        List<Orderitem> orderitemList = new LinkedList<>();

        Orderitem orderItem = new Orderitem();
        orderItem.setProductId(1L);
        orderItem.setCode("AR-12");
        orderItem.setPrice(25.00);
        orderItem.setQuantity(2);

        orderitemList.add(orderItem);

        order1.setOrderItemList(orderitemList);

        calendar.set(2018,12,01,17,00);

        order2.setId(2l);
        order2.setTotal(24.95);
        order2.setDate(calendar.getTime());

        orderList.add(order1);
        orderList.add(order2);
    }

    @Test
    void WhenFindBetweenDates_thenReturnDTOList() {

        //Given
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018,01,01,13,00);
        Date start = calendar.getTime();

        calendar.set(2018,12,01,13,00);
        Date end= calendar.getTime();

        //When
        when(orderRepository.findByDateBetween(start,end)).thenReturn(orderList);
        List<OrderDTO> orderDTOList = orderService.findBetweenDates(start,end);

        //Then
        assertEquals(2,orderDTOList.size());
        verify(orderRepository,times(1)).findByDateBetween(start,end);
    }

    @Test
    void WhenFindAll_ThenReturnTwo() {

        //Given

        //When
        when(orderRepository.findAll()).thenReturn(orderList);
        List<OrderDTO> orderDTOList = orderService.findAll();

        //Then
        assertEquals(2,orderDTOList.size());
    }

    @Test
    void WhenNewOrder_ThenTotalIsEqual() {
        //Given
        Order order = new Order();

        order.setId(1l);
        order.setTotal(10.50);
        order.setDate(calendar.getTime());

        List<Orderitem> orderitemList = new LinkedList<>();

        Orderitem orderItem = new Orderitem();
        orderItem.setProductId(1L);
        orderItem.setCode("AE-12");
        orderItem.setName("Donec vel diam");
        orderItem.setPrice(5.25);
        orderItem.setQuantity(2);

        orderitemList.add(orderItem);
        order.setOrderItemList(orderitemList);

        Product product = new Product();
        product.setId(1L);
        product.setName("Donec vel diam");
        product.setPrice(5.25);
        product.setCode("AE-12");

        Order savedOrder = new Order();
        savedOrder.setDate(order.getDate());
        savedOrder.setTotal(order.getTotal());

        when(orderRepository.save(any(Order.class))).thenReturn(savedOrder);
        when(productService.findProductById(1L)).thenReturn(productMapper.productToProductDto(product));

        //When
        OrderDTO savedOrderDto = orderService.createOrder(orderMapper.orderToOrderDto(order));

        //Then
        assertEquals(order.getTotal(),savedOrderDto.getTotal());
    }

    @Test
    void WhenRecalculateOrder_ThenSameTotal() {

        //Given
        OrderDTO orderDto = orderMapper.orderToOrderDto(orderList.get(0));
        Order order = orderMapper.orderDtoToOrder(orderDto);

        //When
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
        when(orderRepository.save(any(Order.class))).thenReturn(order);

        OrderDTO savedOrderDto = orderService.reCalculateOrderTotal(order.getId());

        //Then
        assertEquals(orderDto.getTotal(),savedOrderDto.getTotal());
    }
}