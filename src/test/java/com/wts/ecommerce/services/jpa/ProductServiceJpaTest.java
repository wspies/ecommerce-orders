package com.wts.ecommerce.services.jpa;

import com.wts.ecommerce.api.v1.mapper.ProductMapper;
import com.wts.ecommerce.api.v1.model.ProductDTO;
import com.wts.ecommerce.models.Order;
import com.wts.ecommerce.models.Product;
import com.wts.ecommerce.repositories.ProductRepository;
import com.wts.ecommerce.services.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class ProductServiceJpaTest {

    ProductService productService;

    @Mock
    ProductRepository productRepository;

    ProductMapper productMapper = ProductMapper.INSTANCE;

    List<Product> productList = new LinkedList<>();

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);
        productService = new ProductServiceJpa(productRepository,productMapper);

        Product product = new Product();
        product.setName("Donec vel diam");
        product.setPrice(5.25);
        product.setCode("AE-12");

        productList.add(product);

    }

    @Test
    void WhenFindAll_ThenReturn1() {

        //Given
        when(productRepository.findAll()).thenReturn(productList);

        //When
        List<ProductDTO> productDTOList = productService.findall();

        //Then
        assertEquals(1,productDTOList.size());
    }

    @Test
    void WhenUpdateProduct_ThenReturnIsEqual() {

        //Given
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(1l);
        productDTO.setPrice(6.99);
        productDTO.setCode("TE-23");
        productDTO.setName("Donec vel diam");

        Product savedProduct = new Product();
        savedProduct.setId(productDTO.getId());
        savedProduct.setName(productDTO.getName());
        savedProduct.setPrice(productDTO.getPrice());
        savedProduct.setCode(productDTO.getCode());

        //When
        when(productRepository.save(any(Product.class))).thenReturn(savedProduct);
        ProductDTO savedProductDTO = productService.updateProduct(1L,productDTO);

        //Then
        assertEquals(productDTO.getName(),savedProductDTO.getName());
        assertEquals(productDTO.getPrice(),savedProductDTO.getPrice());
    }

    @Test
    void WhenNewProduct_ThenReturnIsEqual() {

        //Given
        ProductDTO productDTO = productMapper.productToProductDto(productList.get(0));
        productDTO.setProductUrl(productService.getUrl(productDTO.getId()));

        Product savedProduct = new Product();
        savedProduct.setCode(productDTO.getCode());
        savedProduct.setPrice(productDTO.getPrice());
        savedProduct.setName(productDTO.getName());

        //When
        when(productRepository.save(any(Product.class))).thenReturn(savedProduct);

        ProductDTO createdProduct = productService.createProduct(productDTO);

        //Then
        assertEquals(productDTO,createdProduct);
    }

    @Test
    void WhenFindById_Return() {
        //Given

        Product product = new Product();
        product.setName("Donec vel diam");
        product.setPrice(5.25);
        product.setCode("AE-12");

        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));
        ProductDTO result = productService.findProductById(1l);


        //Then
        ProductDTO productDTO = productMapper.productToProductDto(product);
        productDTO.setProductUrl(productService.getUrl(productDTO.getId()));
        assertEquals(productDTO,result);
    }

    @Test
    void WhenDeleteById_ThenNoExcption() {

        //When
        productService.deleteProductById(1L);

    }
}