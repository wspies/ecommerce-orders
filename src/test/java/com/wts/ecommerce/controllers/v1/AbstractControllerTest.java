package com.wts.ecommerce.controllers.v1;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AbstractControllerTest {

    public static String toJson(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
