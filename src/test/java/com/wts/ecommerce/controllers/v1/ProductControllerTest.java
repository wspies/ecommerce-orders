package com.wts.ecommerce.controllers.v1;

import com.wts.ecommerce.api.v1.model.ProductDTO;
import com.wts.ecommerce.models.Product;
import com.wts.ecommerce.services.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ProductControllerTest extends AbstractControllerTest{

    @Mock
    ProductService productService;

    @InjectMocks
    ProductController productController;

    MockMvc mockMvc;

    List<ProductDTO> productDTOList = new ArrayList<>();

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(productController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();

        ProductDTO product = new ProductDTO();
        product.setId(1L);
        product.setName("Donec vel diam");
        product.setPrice(5.25);
        product.setCode("AE-12");

        productDTOList.add(product);

    }

    @Test
    void WhenGetAllProduct_ThenHttp200AndSize1() throws Exception{

        //Given

        //When
        when(productService.findall()).thenReturn(productDTOList);

        //Then
        mockMvc.perform(get(ProductController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.products",hasSize(1)));

    }

    @Test
    void createNewProduct() throws Exception {

        //Given
        ProductDTO productDTO = productDTOList.get(0);

        //When
        when(productService.createProduct(productDTO)).thenReturn(productDTO);

        //Then
        mockMvc.perform(post(ProductController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(productDTO)))
                .andExpect(status().isCreated());

    }

    @Test
    void updateProduct() throws Exception {

        //Given
        ProductDTO productDTO = productDTOList.get(0);

        //When
        when(productService.updateProduct(anyLong(),any(ProductDTO.class))).thenReturn(productDTO);

        //Then
        mockMvc.perform(put(ProductController.BASE_URL +"/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(productDTO)))
                .andExpect(status().isOk());
    }

    @Test
    void WhenFindById_ThenHttp200AndSize1() throws Exception{

        //Given
        ProductDTO productDTO = productDTOList.get(0);

        //When
        when(productService.findProductById(anyLong())).thenReturn(productDTO);

        //Then
        mockMvc.perform(get(ProductController.BASE_URL + "/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    void WhenDeleteById_ThenReturHttp200() throws Exception{

        mockMvc.perform(delete(ProductController.BASE_URL + "/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
}