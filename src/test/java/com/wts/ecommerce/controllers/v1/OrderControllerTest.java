package com.wts.ecommerce.controllers.v1;

import com.wts.ecommerce.api.v1.mapper.OrderMapper;
import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.api.v1.model.OrderListDTO;
import com.wts.ecommerce.models.Order;
import com.wts.ecommerce.services.OrderService;
import com.wts.ecommerce.services.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OrderControllerTest extends AbstractControllerTest{

    @Mock
    OrderService orderService;

    @InjectMocks
    OrderController orderController;

    MockMvc mockMvc;

    List<OrderDTO> orderListDTO = new ArrayList<>();

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(orderController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();

        OrderMapper orderMapper = OrderMapper.INSTANCE;

        Order order1 = new Order();
        Order order2 = new Order();

        Calendar calendar = Calendar.getInstance();
        calendar.set(2018,01,01,13,00);

        order1.setId(1l);
        order1.setTotal(5.25);
        order1.setDate(calendar.getTime());
        order1.setEmail("test@test.com");

        calendar.set(2018,12,01,17,00);

        order2.setId(2l);
        order2.setTotal(24.95);
        order2.setDate(calendar.getTime());
        order1.setEmail("test@lorum.com");

        orderListDTO.add(orderMapper.orderToOrderDto(order1));
        orderListDTO.add(orderMapper.orderToOrderDto(order2));
    }

    @Test
    void WhenGetAllOrders_ThenHttp200() throws Exception{
        //Given

        //When
        when(orderService.findAll()).thenReturn(orderListDTO);

        //Then
        mockMvc.perform(get(OrderController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    void WhenGetAllOrders_ThenReturnTwoOrders() throws Exception{
        //Given

        //When
        when(orderService.findAll()).thenReturn(orderListDTO);

        //Then
        mockMvc.perform(get(OrderController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders",hasSize(2)));

    }

    @Test
    void WhenNoOrders_ThenThrowException() throws Exception{
        //Given

        //When
        when(orderService.findOrderById(anyLong())).thenThrow(ResourceNotFoundException.class);

        //Then
        mockMvc.perform(get(OrderController.BASE_URL + "12354")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    void createNewOrder() throws Exception{

        OrderDTO orderDTO = orderListDTO.get(0);

        OrderDTO returnDto = new OrderDTO();
        returnDto.setDate(orderDTO.getDate());
        returnDto.setTotal(orderDTO.getTotal());
        returnDto.setEmail(orderDTO.getEmail());

        when(orderService.createOrder(orderDTO)).thenReturn(returnDto);

        mockMvc.perform(post(OrderController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(orderDTO)))
                .andExpect(status().isCreated());

    }

    @Test
    void WhenGetBetweenDate_ThenReturnTwo() throws Exception{

        when(orderService.findBetweenDates(any(Date.class), any(Date.class))).thenReturn(orderListDTO);

        mockMvc.perform(get(OrderController.BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .param("startdate", "2018-01-01")
            .param("enddate", "2018-12-31"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.orders",hasSize(2)));

    }

    @Test
    void WhenCalculateOrder_ThenExpectTotal525() throws Exception{

        //Given
        when(orderService.reCalculateOrderTotal(anyLong())).thenReturn(orderListDTO.get(0));

        //When
        mockMvc.perform(put(OrderController.BASE_URL + "/{id}/calculate",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total",is(5.25)));


    }
}