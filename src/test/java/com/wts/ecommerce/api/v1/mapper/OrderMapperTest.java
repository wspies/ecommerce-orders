package com.wts.ecommerce.api.v1.mapper;

import com.wts.ecommerce.api.v1.model.OrderDTO;
import com.wts.ecommerce.models.Order;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class OrderMapperTest {

    OrderMapper orderMapper = OrderMapper.INSTANCE;

    @Test
    public void MapOrderToOrderDTO(){

        Order order = new Order();
        order.setId(1l);
        order.setTotal(5.25);

        OrderDTO orderDTO = orderMapper.orderToOrderDto(order);

        assertEquals(order.getId(),orderDTO.getId());
        assertEquals(order.getTotal(),orderDTO.getTotal());

    }

}