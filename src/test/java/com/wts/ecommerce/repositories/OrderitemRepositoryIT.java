package com.wts.ecommerce.repositories;

import com.wts.ecommerce.models.Orderitem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class OrderitemRepositoryIT {

    @Autowired
    OrderitemRepository orderitemRepository;

    @BeforeEach
    void setUp() {

        Orderitem orderitem = new Orderitem();
        orderitem.setId(1l);
        orderitem.setProductId(1l);
        orderitem.setQuantity(2);
        orderitem.setCode("123546");
        orderitem.setName("consectetur adipiscing elit");
        orderitem.setPrice(5.25);

        orderitemRepository.save(orderitem);
    }


    @Test
    void whenFindAll_ThenReturnOne(){

        List<Orderitem> orderitemList = orderitemRepository.findAll();

        assertEquals(4,orderitemList.size());

    }
}