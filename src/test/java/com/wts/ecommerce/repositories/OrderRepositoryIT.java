package com.wts.ecommerce.repositories;

import com.wts.ecommerce.models.Order;
import com.wts.ecommerce.services.jpa.OrderServiceJpa;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class OrderRepositoryIT {

    Order order1 = new Order();
    Order order2 = new Order();

    @Autowired
    OrderRepository orderRepository;

    @BeforeEach
    void setUp() {

        Calendar calendar = Calendar.getInstance();
        calendar.set(2018,01,01,13,00);

        order1.setId(100l);
        order1.setEmail("test@test.com");
        order1.setTotal(5.25);
        order1.setDate(calendar.getTime());

        calendar.set(2018,12,01,17,00);

        order2.setId(200l);
        order2.setEmail("test@test.com");
        order2.setTotal(24.95);
        order2.setDate(calendar.getTime());

        orderRepository.save(order1);
        orderRepository.save(order2);
    }


    @Test
    void whenFindBetweenDates_ThenReturnFour() {

        //Given
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 01, 01, 01, 00);
        Date start = calendar.getTime();
        calendar.set(2018, 12, 01, 17, 00);
        Date end = calendar.getTime();

        //When
        List<Order> orderList = orderRepository.findByDateBetween(start,end);

        //Then
        assertEquals(4,orderList.size());
    }

    @Test
    void whenFindBetweenDates_ThenReturnOne() {

        //Given
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 01, 01, 01, 00);
        Date start = calendar.getTime();
        calendar.set(2018, 02, 01, 17, 00);
        Date end = calendar.getTime();

        //When
        List<Order> orderList = orderRepository.findByDateBetween(start,end);

        //Then
        assertEquals(1,orderList.size());
    }
}