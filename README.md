#Spring boot REST service for order entry  

A basic order entry service accessible via a REST interface. Orders have a list of products. Prices of products can be changed 
without affecting order totals. Various ways to search and execute CRUD operations on orders and products. 

####How to build / run

Requirements to build:
* Java 1.8
* Maven 3.0

From the commandline  
* build the project :mvn install  
* run the project: java -jar ecommerce-0.0.1-SNAPSHOT.jar  

Via Postman, curl or any other tool execute a GET http://localhost:8080/api/v1/products/

####Supported actions 

1. CRUD operations on products  
2. Create a Order
3. Read all Orders (between two dates)
4. Recalculate order total 

####Endpoint documentation

Endpoint documentation is available via http://localhost:8080/swagger-ui.html

####Unit testing

Unit tests cover about 90% of the code. Additionally a couple of integration tests have been added 
to test the persistence layer. 

####Storage solution

This solution uses the H2 in memory database via JPA. For testing convenience, on startup some basic test data is 
already loaded into H2. By changing the application.properties file its possibly to swap H2 for any other 
relational database. 

H2 console is accessible via http://localhost:8080/h2-console/ 

####Considerations
The solution is split up in three layers: controller/service/model

1. Controller just receives the http requests and forwards these to the service layer
2. Service layer contains the business logic(calculate the order total, specifying search criteria)
3. Model layer implemented is based on JPA to handle persistance / CRUD operations. 

The used models are not directly exposed to the front end. Instead DTO objects, mapped via Mapstruct, are used. In 
this solution DTOs just used to add some basic HATEOAS support. With some
extra time, a separate DTO for posting an order could be implemented so that only the product id and quantity is 
allowed to be posted when creating a new order.








  
  


